<?php

namespace CustomIS\DoctrineBundle\Doctrine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait UuidIdTrait
 *
 * @package CustomIS\DoctrineBundle\Doctrine
 */
trait UuidIdTrait
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
