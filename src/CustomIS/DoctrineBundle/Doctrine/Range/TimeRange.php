<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Range;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class TimeRange
 */
class TimeRange extends DateRange
{
    /**
     * @return string
     */
    public function __toString(): string
    {
        return '['
               .($this->getStart() === null ? '' : $this->getStart()->format('H:i:s'))
               .', '
               .($this->getEnd() === null ? '' : $this->getEnd()->format('H:i:s'))
               .']';
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed                     $payload
     */
    public function validate(ExecutionContextInterface $context, $payload): void
    {
        if ($this->getStart() > $this->getEnd()) {
            $context->buildViolation('Čas od nesmí být větší než čas do')
                    ->atPath('start')
                    ->addViolation();
        }
    }
}
