<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Range;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class DateRange
 */
class DateRange
{
    /**
     * @var \DateTime|null
     */
    private $start;

    /**
     * @var \DateTime|null
     */
    private $end;

    /**
     * @var bool
     */
    private $endExcluded = false;

    /**
     * DateRange constructor.
     *
     * @param \DateTimeInterface|null $start
     * @param \DateTimeInterface|null $end
     */
    public function __construct(?\DateTimeInterface $start = null, ?\DateTimeInterface $end = null)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '['
               .($this->getStart() === null
                ? ''
                : $this->getStart()->format('Y-m-d'))
               .', '
               .($this->getEnd() === null
                ? ''
                : $this->getEnd()->format('Y-m-d'))
               .($this->endExcluded === true
                ? ')'
                : ']');
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    /**
     * @param \DateTimeInterface|null $start
     */
    public function setStart(?\DateTimeInterface $start = null): void
    {
        $this->start = $start;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    /**
     * @param \DateTimeInterface|null $end
     */
    public function setEnd(?\DateTimeInterface $end = null): void
    {
        $this->end = $end;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed                     $payload
     *
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload): void
    {
        if ($this->start !== null && $this->end !== null && $this->start > $this->end) {
            $context->buildViolation('Datum od nesmí být větší než datum do')
                    ->atPath('start')
                    ->addViolation();
        }
    }

    /**
     * @return bool
     */
    public function isEndExcluded(): bool
    {
        return $this->endExcluded;
    }

    /**
     * @param bool $endExcluded
     */
    public function setEndExcluded(bool $endExcluded): void
    {
        $this->endExcluded = $endExcluded;
    }
}
