<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Range;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class IntRange
 */
class IntRange
{
    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $end;

    /**
     * IntRange constructor.
     *
     * @param int|null $start
     * @param int|null $end
     */
    public function __construct(?int $start = null, ?int $end = null)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getStart(): ?int
    {
        return $this->start;
    }

    /**
     * @param int|null $start
     */
    public function setStart(?int $start = null): void
    {
        $this->start = $start;
    }

    /**
     * @return int
     */
    public function getEnd(): ?int
    {
        return $this->end;
    }

    /**
     * @param int|null $end
     */
    public function setEnd(?int $end = null): void
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '['
               .($this->getStart() ?? '')
               .', '
               .($this->getEnd() ?? '')
               .']';
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed                     $payload
     *
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->start !== null && $this->end !== null && $this->start > $this->end) {
            $context->buildViolation('Od nesmí být větší než do')
                    ->atPath('start')
                    ->addViolation();
        }
    }
}
