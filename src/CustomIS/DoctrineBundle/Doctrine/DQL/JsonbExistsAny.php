<?php

declare(strict_types = 1);

namespace CustomIS\PostgresBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class JsonbExistsAny
 *
 * JsonbExistsAny ::= JSONB_EA(LeftHandSide,{'RightHandSide1','RightHandSide2',...})
 *
 * Will be converted to: "jsonb_exists_any( LeftHandSide, array['RightHandSide1','RightHandSide2',...] )"
 */
class JsonbExistsAny extends FunctionNode
{
    /**
     * @var Node
     */
    public $leftHandSide;

    /**
     * @var Node[]
     */
    public $rightHandSide = [];

    /**
     * @param Parser $parser
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->leftHandSide = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $parser->match(Lexer::T_OPEN_CURLY_BRACE);
        $this->rightHandSide[] = $parser->ScalarExpression();
        $lexer = $parser->getLexer();
        while ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);

            $this->rightHandSide[] = $parser->ScalarExpression();
        }
        $parser->match(Lexer::T_CLOSE_CURLY_BRACE);
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return ' jsonb_exists_any('
               .$this->leftHandSide->dispatch($sqlWalker)
               .',array['.
               implode(
                   ',',
                   array_map(function ($rh) use ($sqlWalker) {
                       return $rh->dispatch($sqlWalker);
                   }, $this->rightHandSide)
               )
               .'])';
    }
}
