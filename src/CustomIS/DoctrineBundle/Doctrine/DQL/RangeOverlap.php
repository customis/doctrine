<?php

declare(strict_types = 1);

namespace CustomIS\PostgresBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class RangeOverlap
 */
class RangeOverlap extends FunctionNode
{
    /**
     * @var Node
     */
    public $rangeOne;

    /**
     * @var Node
     */
    public $rangeTwo;

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return '('
               .$this->rangeOne->dispatch($sqlWalker)
               .' && '
               .$this->rangeTwo->dispatch($sqlWalker)
               .')';
    }

    /**
     * @param Parser $parser
     *
     * @return void
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->rangeOne = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->rangeTwo = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
