<?php

declare(strict_types = 1);

namespace CustomIS\PostgresBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class IntRange
 */
class IntRange extends FunctionNode
{
    /**
     * @var Node
     */
    public $od;

    /**
     * @var Node
     */
    public $do;

    /**
     * @var Node
     */
    public $type;

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'int4range('
               .$this->od->dispatch($sqlWalker)
               .', '
               .$this->do->dispatch($sqlWalker)
               .','
               .$this->type->dispatch($sqlWalker)
               .')';
    }

    /**
     * @param Parser $parser
     *
     * @return void
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->od = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->do = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->type = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
