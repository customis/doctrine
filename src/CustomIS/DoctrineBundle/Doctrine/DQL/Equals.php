<?php

declare(strict_types = 1);

namespace CustomIS\PostgresBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class Equals
 */
class Equals extends FunctionNode
{
    /**
     * @var Node
     */
    private $exprOne;

    /**
     * @var Node
     */
    private $exprTwo;

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return $this->exprOne->dispatch($sqlWalker).' = '.$this->exprTwo->dispatch($sqlWalker);
    }

    /**
     * @param Parser $parser
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->exprOne = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->exprTwo = $parser->StringExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
