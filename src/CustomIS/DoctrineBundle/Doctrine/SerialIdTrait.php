<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait SerialIdTrait
 *
 * @package CustomIS\DoctrineBundle\Doctrine
 */
trait SerialIdTrait
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
