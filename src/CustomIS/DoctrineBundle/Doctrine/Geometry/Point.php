<?php

namespace CustomIS\DoctrineBundle\Doctrine\Geometry;

/**
 * Class Point
 *
 * @package CustomIS\DoctrineBundle\Geometry
 */
class Point implements \JsonSerializable
{
    /**
     * @var float
     */
    private $x;

    /**
     * @var float
     */
    private $y;

    /**
     * Point constructor.
     *
     * @param float $x
     * @param float $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @param bool $inverse
     *
     * @return array
     */
    public function toArray(bool $inverse = false)
    {
        return [
            $inverse === true ? $this->y : $this->x,
            $inverse === true ? $this->x : $this->y,
        ];
    }

    /**
     * @param array $point
     *
     * @return Point
     */
    public static function fromArray(array $point)
    {
        return new self($point[0], $point[1]);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *        which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
