<?php

namespace CustomIS\DoctrineBundle\Dcotrine\Geometry;

/**
 * Class Polygon
 *
 * @package CustomIS\DoctrineBundle\Geometry
 */
class Polygon implements \JsonSerializable
{
    /**
     * @var Point[]
     */
    private $points = [];

    /**
     * Polygon constructor.
     *
     * @param array|Point[] $points
     */
    public function __construct(array $points = [])
    {
        $this->points = $points;
    }

    /**
     * @param Point $point
     */
    public function addPoint(Point $point)
    {
        $this->points[] = $point;
    }

    /**
     * @return Point[]
     */
    public function getPoints(): array
    {
        return $this->points;
    }

    /**
     * @param bool $inversePoints
     *
     * @return array|\float[][]
     */
    public function toArray(bool $inversePoints = false)
    {
        return array_map(function (Point $point) use ($inversePoints) {
            return $point->toArray($inversePoints);
        }, $this->points);
    }

    /**
     * @param array $arrayOfPoints
     *
     * @return Polygon
     */
    public static function fromArray(array $arrayOfPoints)
    {
        return new self(array_map(function (array $point) {
            return Point::fromArray($point);
        }, $arrayOfPoints));
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *        which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->points;
    }
}
