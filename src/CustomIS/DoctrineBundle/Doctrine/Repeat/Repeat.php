<?php

namespace CustomIS\DoctrineBundle\Repeat;

class Repeat
{
    /**
     * @var int
     */
    private $times;

    /**
     * @var \DateInterval;
     */
    private $interval;

    /**
     * Repeat constructor.
     *
     * @param int           $times
     * @param \DateInterval $interval
     */
    public function __construct(int $times, \DateInterval $interval)
    {
        $this->times = $times;
        $this->interval = $interval;
    }

    /**
     * @return int
     */
    public function getTimes(): int
    {
        return $this->times;
    }

    /**
     * @return \DateInterval
     */
    public function getInterval(): \DateInterval
    {
        return $this->interval;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            "(%d, '%s')",
            $this->times,
            $this->interval->format('P%yY%mM%dD%hH%iM%sS')
        );
    }
}
