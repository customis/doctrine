<?php

namespace CustomIS\DoctrineBundle\Doctrine\ORM\Query;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Query\ResultSetMappingBuilder as ResultSetMappingBuilderBase;

/**
 * Class ResultSetMappingBuilder
 *
 * @package CustomIS\DoctrineBundle\Doctrine\ORM\Query
 */
class ResultSetMappingBuilder extends ResultSetMappingBuilderBase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Default column renaming mode.
     *
     * @var int
     */
    private $defaultRenameMode;

    /**
     * @var int
     */
    private $sqlCounter = 0;

    /**
     * @var array
     */
    private $subclassAlias = [];

    /**
     * @param EntityManagerInterface $em
     * @param integer                $defaultRenameMode
     */
    public function __construct(EntityManagerInterface $em, $defaultRenameMode = self::COLUMN_RENAMING_NONE)
    {
        parent::__construct($em, $defaultRenameMode);
        $this->em = $em;
        $this->defaultRenameMode = $defaultRenameMode;
    }

    /**
     * @param string $class
     * @param string $alias
     * @param array  $renamedColumns
     * @param null   $renameMode
     * @param array  $subclassAlias
     */
    public function addRootEntityFromClassMetadata(
        $class,
        $alias,
        $renamedColumns = [],
        $renameMode = null,
        array $subclassAlias = []
    ): void {
        $renameMode = $renameMode
            ?: $this->defaultRenameMode;
        $columnAliasMap = $this->getColumnAliasMap($class, $renameMode, $renamedColumns);

        $this->addEntityResult($class, $alias);
        $this->addAllClassFields($class, $alias, $columnAliasMap);
        $this->addAllInheritanceClassFields($class, $alias, $renameMode, $renamedColumns);
        $this->subclassAlias[$alias] = $subclassAlias;
    }

    /**
     * @param string $class
     * @param string $alias
     * @param string $parentAlias
     * @param string $relation
     * @param array  $renamedColumns
     * @param null   $renameMode
     * @param array  $subclassAlias
     */
    public function addJoinedEntityFromClassMetadata(
        $class,
        $alias,
        $parentAlias,
        $relation,
        $renamedColumns = [],
        $renameMode = null,
        array $subclassAlias = []
    ) {
        $renameMode = $renameMode
            ?: $this->defaultRenameMode;
        $columnAliasMap = $this->getColumnAliasMap($class, $renameMode, $renamedColumns);

        $this->addJoinedEntityResult($class, $alias, $parentAlias, $relation);
        $this->addAllClassFields($class, $alias, $columnAliasMap);
        $this->addAllInheritanceClassFields($class, $alias, $renameMode, $renamedColumns);
        $this->subclassAlias[$alias] = $subclassAlias;
    }

    /**
     * @param array $tableAliases
     *
     * @return string
     */
    public function generateSelectClause($tableAliases = [])
    {
        $sql = '';

        foreach ($this->columnOwnerMap as $columnName => $dqlAlias) {
            $tableAlias = $tableAliases[$dqlAlias] ?? $dqlAlias;

            if ($sql) {
                $sql .= ', ';
            }

            $column = '';
            if (isset($this->fieldMappings[$columnName])) {
                $class = $this->em->getClassMetadata($this->declaringClasses[$columnName]);
                if (isset($this->subclassAlias[$dqlAlias][$class->getName()])) {
                    $tableAlias = $this->subclassAlias[$dqlAlias][$class->getName()];
                }

                $column = $class->fieldMappings[$this->fieldMappings[$columnName]]['columnName'];
                if (isset($class->fieldMappings[$this->fieldMappings[$columnName]]['requireSQLConversion'])) {
                    $type = Type::getType($class->getTypeOfField($this->fieldMappings[$columnName]));
                    $column = $tableAlias.'.'.$column;
                    $column = $type->convertToPHPValueSQL($column, $this->em->getConnection()->getDatabasePlatform());
                } else {
                    $column = $tableAlias.'.'.$column;
                }
            } else {
                if (isset($this->metaMappings[$columnName])) {
                    if (isset($this->declaringClasses[$columnName])) {
                        $class = $this->em->getClassMetadata($this->declaringClasses[$columnName]);
                        if (isset($this->subclassAlias[$dqlAlias][$class->getName()])) {
                            $tableAlias = $this->subclassAlias[$dqlAlias][$class->getName()];
                        }
                    }
                    $column = $this->metaMappings[$columnName];
                } else {
                    if (isset($this->discriminatorColumns[$dqlAlias])) {
                        $column = $this->discriminatorColumns[$dqlAlias];
                    }
                }
                $column = $tableAlias.'.'.$column;
            }

            $sql .= $column.' AS '.$columnName;
        }

        return $sql;
    }

    /**
     * @param string $class
     * @param string $alias
     * @param array  $columnAliasMap
     */
    protected function addAllClassFields($class, $alias, $columnAliasMap = []): void
    {
        $classMetadata = $this->em->getClassMetadata($class);
        $platform = $this->em->getConnection()->getDatabasePlatform();

        if (!$this->isInheritanceSupported($classMetadata)) {
            throw new \InvalidArgumentException('ResultSetMapping builder does not currently support your inheritance scheme.');
        }

        foreach ($classMetadata->getColumnNames() as $columnName) {
            $propertyName = $classMetadata->getFieldName($columnName);
            $columnAlias = $platform->getSQLResultCasing($columnAliasMap[$columnName]);

            if (isset($this->fieldMappings[$columnAlias])) {
                throw new \InvalidArgumentException("The column '$columnName' conflicts with another column in the mapper.");
            }

            $this->addFieldResult($alias, $columnAlias, $propertyName);
        }

        foreach ($classMetadata->associationMappings as $associationMapping) {
            if ($associationMapping['isOwningSide'] && $associationMapping['type'] & ClassMetadataInfo::TO_ONE) {
                foreach ($associationMapping['joinColumns'] as $joinColumn) {
                    $columnName = $joinColumn['name'];
                    $columnAlias = $platform->getSQLResultCasing($columnAliasMap[$columnName]);

                    if (isset($this->metaMappings[$columnAlias])) {
                        throw new \InvalidArgumentException("The column '$columnAlias' conflicts with another column in the mapper.");
                    }

                    $this->addMetaResult(
                        $alias,
                        $columnAlias,
                        $columnName,
                        isset($associationMapping['id']) && $associationMapping['id'] === true
                    );
                }
            }
        }
    }

    protected function addAllInheritanceClassFields($class, $alias, $renameMode, $renamedColumns)
    {
        $classMetadata = $this->em->getClassMetadata($class);
        $platform = $this->em->getConnection()->getDatabasePlatform();

        if ($classMetadata->isInheritanceTypeJoined()) {
            $this->setDiscriminatorColumn(
                $alias,
                $classMetadata->discriminatorColumn['name'],
                $classMetadata->discriminatorColumn['fieldName']
            );
            $this->addMetaResult(
                $alias,
                $classMetadata->discriminatorColumn['name'],
                $classMetadata->discriminatorColumn['fieldName']
            );

            $parentClassColumnAliasMap = $this->getColumnAliasMap($class, $renameMode, $renamedColumns);

            $columns = [];
            foreach ($classMetadata->subClasses as $subClass) {
                //                $this->aliasMap[$subclassAlias[$subClass]] = $subClass;

                $subClassMetadata = $this->em->getClassMetadata($subClass);
                $subClassColumnAliasMap = $this->getColumnAliasMap($subClass, $renameMode, $renamedColumns);
                $columnAliasMap = array_diff_key($subClassColumnAliasMap, $parentClassColumnAliasMap);

                foreach ($subClassMetadata->getColumnNames() as $subClassColumnName) {
                    if (!in_array($subClassColumnName, $classMetadata->getColumnNames(), true)) {
                        $propertyName = $subClassMetadata->getFieldName($subClassColumnName);
                        $columnAlias = $platform->getSQLResultCasing($columnAliasMap[$subClassColumnName]);
                        $columns[$columnAlias] = $columns[$columnAlias] ?? 0;
                        //                        if (isset($this->fieldMappings[$columnAlias])) {
                        //                            throw new \InvalidArgumentException("The column '$subClassColumnName' conflicts with another column in the mapper.");
                        //                        }

                        //                        $this->addMetaResult($subclassAlias[$subClass], $columnAlias, $propertyName);
                        $this->addFieldResult($alias, $columnAlias.$columns[$columnAlias]++, $propertyName, $subClass);
                    }
                }

                foreach ($subClassMetadata->associationMappings as $associationMappingKey => $associationMapping) {
                    if (!array_key_exists($associationMappingKey, $classMetadata->associationMappings)) {
                        if ($associationMapping['isOwningSide']
                            && $associationMapping['type']
                               & ClassMetadataInfo::TO_ONE
                        ) {
                            foreach ($associationMapping['joinColumns'] as $joinColumn) {
                                $columnName = $joinColumn['name'];
                                $columnAlias = $platform->getSQLResultCasing($columnAliasMap[$columnName]);
                                $columns[$columnAlias] = $columns[$columnAlias] ?? 0;
                                //                                if (isset($this->metaMappings[$columnAlias])) {
                                //                                    throw new \InvalidArgumentException("The column '$columnAlias' conflicts with another column in the mapper.");
                                //                                }

                                $name = $columnAlias.$columns[$columnAlias]++;
                                $this->addMetaResult(
                                    $alias,
                                    $name,
                                    $columnName,
                                    (isset($associationMapping['id']) && $associationMapping['id'] === true)
                                );
                                $this->declaringClasses[$name] = $subClass;
                            }
                        }
                    }
                }
            }
        }
    }

    private function getColumnAliasMap($className, $mode, array $customRenameColumns)
    {
        if ($customRenameColumns) { // for BC with 2.2-2.3 API
            $mode = self::COLUMN_RENAMING_CUSTOM;
        }

        $columnAlias = [];
        $class = $this->em->getClassMetadata($className);

        foreach ($class->getColumnNames() as $columnName) {
            $columnAlias[$columnName] = $this->getColumnAlias($columnName, $mode, $customRenameColumns);
        }

        foreach ($class->associationMappings as $associationMapping) {
            if ($associationMapping['isOwningSide'] && $associationMapping['type'] & ClassMetadataInfo::TO_ONE) {
                foreach ($associationMapping['joinColumns'] as $joinColumn) {
                    $columnName = $joinColumn['name'];
                    $columnAlias[$columnName] = $this->getColumnAlias($columnName, $mode, $customRenameColumns);
                }
            }
        }

        return $columnAlias;
    }

    private function getColumnAlias($columnName, $mode, array $customRenameColumns)
    {
        switch ($mode) {
            case self::COLUMN_RENAMING_INCREMENT:
                return $columnName.$this->sqlCounter++;

            case self::COLUMN_RENAMING_CUSTOM:
                return isset($customRenameColumns[$columnName])
                    ? $customRenameColumns[$columnName]
                    : $columnName;

            case self::COLUMN_RENAMING_NONE:
                return $columnName;
        }
    }

    private function isInheritanceSupported(ClassMetadataInfo $classMetadata)
    {
        if (($classMetadata->isInheritanceTypeSingleTable() || $classMetadata->isInheritanceTypeJoined())
            && in_array($classMetadata->name, $classMetadata->discriminatorMap, true)
        ) {
            return true;
        }

        return !($classMetadata->isInheritanceTypeSingleTable() || $classMetadata->isInheritanceTypeJoined());
    }
}
