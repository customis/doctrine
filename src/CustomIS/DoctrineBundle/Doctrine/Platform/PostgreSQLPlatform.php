<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Platform;

use Doctrine\DBAL\Platforms\PostgreSQL92Platform;

/**
 * Class PostgreSQLPlatform
 */
class PostgreSQLPlatform extends PostgreSQL92Platform
{
    /**
     * @return string
     */
    public function getDateTimeFormatString(): string
    {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * @param array $fieldDeclaration
     *
     * @return string
     */
    public function getDateTimeTypeDeclarationSQL(array $fieldDeclaration): string
    {
        return 'TIMESTAMP WITHOUT TIME ZONE';
    }

    /**
     * @param array $fieldDeclaration
     *
     * @return string
     */
    public function getDateTimeTzTypeDeclarationSQL(array $fieldDeclaration): string
    {
        return 'TIMESTAMP WITH TIME ZONE';
    }

    /**
     * @return void
     */
    protected function initializeDoctrineTypeMappings(): void
    {
        parent::initializeDoctrineTypeMappings();
        $this->doctrineTypeMapping['repeat'] = 'repeat';
        $this->doctrineTypeMapping['_numeric'] = 'numeric[]';
        $this->doctrineTypeMapping['_varchar'] = 'varchar[]';
    }
}
