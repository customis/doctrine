<?php

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Money\Parser\DecimalMoneyParser;

/**
 * Class CZKCurrencyType
 */
class CZKCurrencyType extends Type
{
    const CZKCURRENCY = 'czkcurrency';

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'numeric(10,2)';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Money
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Money
    {
        if (null !== $value) {
            $moneyParser = new DecimalMoneyParser(new ISOCurrencies());

            return $moneyParser->parse($value, 'CZK');
        }

        return null;
    }

    /**
     * @param mixed|Money      $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof Money) {
            $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());

            return $moneyFormatter->format($value);
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::CZKCURRENCY;
    }
}
