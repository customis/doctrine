<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Doctrine\Range\DateRange;
use CustomIS\DoctrineBundle\Doctrine\Range\TimeRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class TimeRangeType
 */
class TimeRangeType extends Type
{
    const DATE_RANGE = 'timerange'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'timerange';
    }

    /**
     * @param string           $value
     * @param AbstractPlatform $platform
     *
     * @return TimeRange|null
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?TimeRange
    {
        if (null === $value) {
            return null;
        }

        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9:]*?),(?P<upper_bound>[0-9:]*?)(?P<upper_bracket>[\])])$~', $value, $matches)) {
            throw new \RuntimeException('Not matching timerange format');
        }

        $lowerBound = null;
        if (!empty($matches['lower_bound'])) {
            $lowerBound = new \DateTime($matches['lower_bound']);
        }

        $upperBound = null;
        if (!empty($matches['upper_bound'])) {
            $upperBound = new \DateTime($matches['upper_bound']);
        }

        return new TimeRange($lowerBound, $upperBound);
    }

    /**
     * @param DateRange|null   $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof TimeRange) {
            $end = '';
            if (null !== $value->getEnd()) {
                $end = $value->getEnd()->format('H:i:s');
            }

            return '['
                   .($value->getStart() === null ? '' : $value->getStart()->format('H:i:s'))
                   ."',"
                   .$end
                   .']';
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::DATE_RANGE;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        return "'[' || COALESCE(lower("
               .$sqlExpr
               .")::varchar, '') || ',' || COALESCE(upper("
               .$sqlExpr
               .")::varchar,'') ||']'";
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return ['timerange'];
    }
}
