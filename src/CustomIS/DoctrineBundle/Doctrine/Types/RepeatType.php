<?php

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Repeat\Repeat;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class RepeatType
 *
 * SQL declaration: CREATE TYPE repeat AS (times INTEGER, interval INTERVAL);
 */
class RepeatType extends Type
{
    const REPEAT = 'repeat';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array                                     $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform         The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return self::REPEAT;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName(): string
    {
        return self::REPEAT;
    }

    /**
     * @param Repeat|mixed     $value
     * @param AbstractPlatform $platform
     *
     * @return string|mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof RepeatType) {
            $intervalType = Type::getType('interval');

            return sprintf(
                "(%d, '%s')",
                $value->getTimes(),
                $intervalType->convertToDatabaseValue($value->getInterval(), $platform)
            );
        }

        return $value;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        return "CAST($sqlExpr AS repeat)";
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Repeat|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Repeat
    {
        if ('' === $value || null === $value) {
            return null;
        }

        $intervalTyoe = Type::getType('interval');

        $value = trim($value, '()');
        $value = str_getcsv($value);

        return new Repeat(
            $value[0],
            $intervalTyoe->convertToPHPValue($value[1], $platform)
        );
    }
}
