<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use Brick\Math\BigDecimal;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DecimalType as BaseDoctrineDecimalType;

/**
 * Class DecimalType
 */
class DecimalType extends BaseDoctrineDecimalType
{
    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null !== $value) {
            return BigDecimal::of($value);
        }

        return null;
    }
}
