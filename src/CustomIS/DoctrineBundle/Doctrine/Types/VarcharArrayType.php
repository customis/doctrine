<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class VarcharArrayType
 */
class VarcharArrayType extends Type
{
    public const ARRAY_VARCHAR = 'varchar[]';

    /**
     * @return string
     */
    public function getName(): string
    {
        return static::ARRAY_VARCHAR;
    }

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(static::ARRAY_VARCHAR);
    }

    /**
     * @param mixed|array      $array
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($array, AbstractPlatform $platform)
    {
        if (null === $array) {
            return null;
        }

        $convertArray = [];
        foreach ($array as $value) {
            if (!is_string($value)) {
                continue;
            }
            $convertArray[] = (string) $value;
        }

        return '{'.implode(',', $convertArray).'}';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return array|mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        $value = ltrim(rtrim($value, '}'), '{');
        if ('' === $value) {
            return [];
        }

        $r = explode(',', $value);
        foreach ($r as &$v) {
            $v = (string) $v;
        }

        return $r;
    }
}
