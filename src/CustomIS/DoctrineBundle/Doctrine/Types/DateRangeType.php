<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Doctrine\Range\DateRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class DateRangeType
 */
class DateRangeType extends Type
{
    const DATE_RANGE = 'daterange'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'daterange';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return DateRange|null
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DateRange
    {
        if (null === $value) {
            return null;
        }

        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9-]*?),\s*(?P<upper_bound>[0-9-]*?)(?P<upper_bracket>[\])])$~', $value, $matches)) {
            throw new \RuntimeException("Daterange value didn't match");
        }

        $lowerBound = null;
        if (!empty($matches['lower_bound'])) {
            $lowerBound = new \DateTime($matches['lower_bound']);
        }

        $upperBound = null;
        if (!empty($matches['upper_bound'])) {
            $upperBound = new \DateTime($matches['upper_bound']);
        }

        return new DateRange($lowerBound, $upperBound);
    }

    /**
     * @param mixed|DateRange  $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateRange) {
            $end = '';
            if (null !== $value->getEnd()) {
                $end = "'".$value->getEnd()->format('Y-m-d')."'";
            }

            return "['"
                   .($value->getStart() === null ? '' : $value->getStart()->format('Y-m-d'))
                   ."',"
                   .$end
                   .(
                   $value->isEndExcluded() ? ')' : ']'
                   );
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::DATE_RANGE;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        return "
            CASE    WHEN $sqlExpr IS NOT NULL THEN '[' || COALESCE(lower_bound($sqlExpr)::varchar, '') || ',' || COALESCE(upper_bound($sqlExpr)::varchar,'') ||']'
                    ELSE null
            END
        ";
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return ['daterange'];
    }
}
