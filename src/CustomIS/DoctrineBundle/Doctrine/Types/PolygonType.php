<?php

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Doctrine\Geometry\Polygon;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class PolygonType
 */
class PolygonType extends Type
{
    const POLYGON = 'polygon'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'polygon';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Polygon
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Polygon
    {
        if (null !== $value) {
            $polygonArray = json_decode(str_replace(['(', ')'], ['[', ']'], $value));

            return Polygon::fromArray($polygonArray);
        }

        return null;
    }

    /**
     * @param Polygon|mixed    $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof Polygon) {
            return str_replace(['[', ']'], ['(', ')'], json_encode($value->toArray()));
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::POLYGON;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return ['polygon'];
    }
}
