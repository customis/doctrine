<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Doctrine\Range\DateRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class TimestampRangeType
 */
class TimestampRangeType extends Type
{
    const DATE_RANGE = 'tsrange'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'tsrange';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return DateRange|null
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DateRange
    {
        if (null === $value) {
            return null;
        }

        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])"?(?P<lower_bound>[0-9-: .]*?)"?,"?(?P<upper_bound>[0-9-: .]*?)"?(?P<upper_bracket>[\])])$~', $value, $matches)) {
            throw new \RuntimeException('Not matching timestamp range format');
        }

        $lowerBound = null;
        if (!empty($matches['lower_bound'])) {
            $lowerBound = new \DateTime($matches['lower_bound']);
        }

        $upperBoudn = null;
        if (!empty($matches['upper_bound'])) {
            $upperBoudn = new \DateTime($matches['upper_bound']);
        }

        return new DateRange($lowerBound, $upperBoudn);
    }

    /**
     * @param DateRange        $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateRange) {
            $end = '';
            if (null !== $value->getEnd()) {
                $end = $value->getEnd()->format('Y-m-d H:i:s');
            }

            return '['
                   .($value->getStart() === null ? '' : $value->getStart()->format('Y-m-d H:i:s'))
                   ."',"
                   .$end
                   .(
                   $value->isEndExcluded()
                       ? ')'
                       : ']'
                   );
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::DATE_RANGE;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        return "'[' || COALESCE(lower("
               .$sqlExpr
               .")::varchar, '') || ',' || COALESCE(upper("
               .$sqlExpr
               .")::varchar,'') ||']'";
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return ['tsrange', 'tstzrange'];
    }
}
