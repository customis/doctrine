<?php

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Doctrine\Geometry\Point;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class PolygonType
 */
class PointType extends Type
{
    const POINT = 'point'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'point';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return Point|null
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Point
    {
        if (null !== $value) {
            $value = trim($value, '()');
            $pointArray = array_map('trim', explode(',', $value));

            return Point::fromArray($pointArray);
        }

        return null;
    }

    /**
     * @param mixed|Point      $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof Point) {
            return sprintf('(%f,%f)', $value->getX(), $value->getY());
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::POINT;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return ['point'];
    }
}
