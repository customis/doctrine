<?php

namespace CustomIS\DoctrineBundle\Doctrine\Types;

use CustomIS\DoctrineBundle\Range\IntRange;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class IntRangeType
 */
class IntRangeType extends Type
{
    const INT_RANGE = 'intrange'; // modify to match your type name

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'int4range';
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return IntRange
     *
     * @throws \Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): IntRange
    {
        $matches = [];
        if (!preg_match('~^(?P<lower_bracket>[\[(])(?P<lower_bound>[0-9]*?),(?P<upper_bound>[0-9]*?)(?P<upper_bracket>[\])])$~', $value, $matches)) {
            throw new \Exception();
        }

        $lowerBound = null;
        if (!empty($matches['lower_bound'])) {
            $lowerBound = $matches['lower_bound'];
        }

        $upperBound = null;
        if (!empty($matches['upper_bound'])) {
            $upperBound = $matches['upper_bound'];
        }

        return new IntRange($lowerBound, $upperBound);
    }

    /**
     * @param mixed|IntRange   $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof IntRange) {
            $end = '';
            if (!empty($value->getEnd())) {
                $end = $value->getEnd();
            }

            return '['.$value->getStart().','.$end.']';
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::INT_RANGE;
    }

    /**
     * @return bool
     */
    public function canRequireSQLConversion()
    {
        return true;
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        return "'[' || COALESCE(lower("
               .$sqlExpr
               .")::varchar, '') || ',' || COALESCE(upper_bound("
               .$sqlExpr
               .")::varchar,'') ||']'";
    }

    /**
     * @param string           $sqlExpr
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return array
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return ['intrange', 'int4range'];
    }
}
