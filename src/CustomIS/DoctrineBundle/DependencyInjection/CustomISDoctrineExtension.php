<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\DependencyInjection;

use CustomIS\DoctrineBundle\Doctrine\Platform\PostgreSQLPlatform;
use CustomIS\DoctrineBundle\Doctrine\Types\CZKCurrencyType;
use CustomIS\DoctrineBundle\Doctrine\Types\DecimalType;
use CustomIS\DoctrineBundle\Doctrine\Types\IntervalType;
use CustomIS\DoctrineBundle\Doctrine\Types\IntRangeType;
use CustomIS\DoctrineBundle\Doctrine\Types\NumericArrayType;
use CustomIS\DoctrineBundle\Doctrine\Types\PointType;
use CustomIS\DoctrineBundle\Doctrine\Types\PolygonType;
use CustomIS\DoctrineBundle\Doctrine\Types\RepeatType;
use CustomIS\DoctrineBundle\Doctrine\Types\VarcharArrayType;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CustomISDoctrineExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension('doctrine')
            && isset($container->getParameter('kernel.bundles')['DoctrineBundle'])
        ) {
            $config = [
                'dbal' => [
                    'types'            => [
                        CZKCurrencyType::CZKCURRENCY    => CZKCurrencyType::class,
                        IntervalType::INTERVAL          => IntervalType::class,
                        IntRangeType::INT_RANGE         => IntRangeType::class,
                        PointType::POINT                => PointType::class,
                        PolygonType::POLYGON            => PolygonType::class,
                        RepeatType::REPEAT              => RepeatType::class,
                        Type::DECIMAL                   => DecimalType::class,
                        NumericArrayType::ARRAY_NUMERIC => NumericArrayType::class,
                        VarcharArrayType::ARRAY_VARCHAR => VarcharArrayType::class,
                    ],
                    'platform_service' => PostgreSQLPlatform::class,
                ],
            ];

            $container->prependExtensionConfig('doctrine', $config);
        }

        if ($container->hasExtension('twig')) {
            $config = [
                'form_themes' => [
                    dirname(__DIR__).'/Resources/views/Form/fields.html.twig',
                ],
            ];

            $container->prependExtensionConfig('twig', $config);
        }
    }
}
