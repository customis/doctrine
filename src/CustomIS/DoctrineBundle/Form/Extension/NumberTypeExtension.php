<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Form\Extension;

use CustomIS\DoctrineBundle\Form\DataTransformer\NumberToLocalizedStringTransformer;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class NumericTypeExtension
 */
class NumberTypeExtension extends AbstractTypeExtension
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->resetViewTransformers();
        $builder->addViewTransformer(new NumberToLocalizedStringTransformer(
            $options['scale'],
            $options['grouping'],
            $options['rounding_mode']
        ));
    }

    /**
     * @return string
     */
    public function getExtendedType(): string
    {
        return NumberType::class;
    }
}
