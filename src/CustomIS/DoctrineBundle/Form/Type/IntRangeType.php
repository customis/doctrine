<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Form\Type;

use CustomIS\DoctrineBundle\Doctrine\Range\IntRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class IntRangeType
 */
class IntRangeType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $startConstraints = [];
        if (!$options['infinite_start']) {
            $startConstraints[] = new NotBlank();
        }

        $endConstraints = [];
        if (!$options['infinite_end']) {
            $endConstraints[] = new NotBlank();
        }

        $builder
            ->add($options['start_name'], IntegerType::class, [
                'attr'        => [
                    'data-type' => 'start',
                    'title'     => 'Od',
                ],
                'required'    => !$options['infinite_start'],
                'label'       => $options['start_title'],
                'constraints' => $startConstraints,
            ])
            ->add($options['end_name'], IntegerType::class, [
                'attr'        => [
                    'data-type' => 'end',
                    'title'     => 'Do',
                ],
                'required'    => !$options['infinite_end'],
                'label'       => $options['end_title'],
                'constraints' => $endConstraints,
            ])
            ->setDataMapper($this);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => IntRange::class,
            'infinite_start' => false,
            'infinite_end'   => false,
            'start_title'    => 'Od',
            'end_title'      => 'Do',
            'attr'           => [
                'class' => 'customis-form-int-range',
            ],
            'empty_data'     => null,
            'start_name'     => 'start',
            'end_name'       => 'end',
            'constraints'    => new Valid(),
        ]);

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param IntRange                     $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        /** @var FormInterface $parentForm */
        $parentForm = array_values($forms)[0]->getParent();
        if (null !== $data) {
            $forms[$parentForm->getConfig()->getOption('start_name')]->setData($data->getStart() ?? $data->getStart());
            $forms[$parentForm->getConfig()->getOption('end_name')]->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param IntRange                     $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        /** @var FormInterface $parentForm */
        $parentForm = array_values($forms)[0]->getParent();
        $data = new IntRange(
            $forms[$parentForm->getConfig()->getOption('start_name')]->getData(),
            $forms[$parentForm->getConfig()->getOption('end_name')]->getData()
        );
    }
}
