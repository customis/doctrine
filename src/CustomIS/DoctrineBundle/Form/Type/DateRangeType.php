<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Form\Type;

use CustomIS\DoctrineBundle\Doctrine\Range\DateRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class DateRangeType
 */
class DateRangeType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $startConstraints = [];
        if (!$options['infinite_start']) {
            $startConstraints[] = new NotBlank();
        }

        $endConstraints = [];
        if (!$options['infinite_end']) {
            $endConstraints[] = new NotBlank();
        }

        $builder
            ->add('start', DateType::class, array_merge([
                'attr'        => [
                    'data-type' => 'start',
                    'title'     => 'Od',
                ],
                'required'    => !$options['infinite_start'],
                'label'       => $options['start_title'],
                'constraints' => $startConstraints,
                'format'      => 'dd.MM.yyyy',
            ], $options['start_options']))
            ->add('end', DateType::class, array_merge([
                'attr'        => [
                    'data-type' => 'end',
                    'title'     => 'Do',
                ],
                'required'    => !$options['infinite_end'],
                'label'       => $options['end_title'],
                'constraints' => $endConstraints,
                'format'      => 'dd.MM.yyyy',
            ], $options['end_options']))
            ->setDataMapper($this);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => DateRange::class,
            'infinite_start' => false,
            'infinite_end'   => false,
            'start_title'    => 'Od',
            'end_title'      => 'Do',
            'attr'           => [
                'class' => 'customis-form-date-range',
            ],
            'empty_data'     => null,
            'constraints'    => new Valid(),
            'start_options'  => [],
            'end_options'    => [],
        ]);

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param DateRange                    $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if (null !== $data) {
            $forms['start']->setData($data->getStart() ?? $data->getStart());
            $forms['end']->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param DateRange                    $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        if (!($forms['start']->getData() === null && $forms['end']->getData() === null)) {
            $data = new DateRange(
                $forms['start']->getData(),
                $forms['end']->getData()
            );
        }
    }
}
