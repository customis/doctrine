<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Form\Type;

use CustomIS\DoctrineBundle\Doctrine\Range\TimeRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class TimeRangeType
 */
class TimeRangeType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $startConstraints = [];
        if (!$options['infinite_start']) {
            $startConstraints[] = new NotBlank();
        }

        $endConstraints = [];
        if (!$options['infinite_end']) {
            $endConstraints[] = new NotBlank();
        }

        $builder
            ->add('start', TimeType::class, [
                'attr'        => [
                    'data-type' => 'start',
                    'title'     => 'Od',
                ],
                'required'    => !$options['infinite_start'],
                'label'       => $options['start_title'],
                'constraints' => $startConstraints,
            ])
            ->add('end', TimeType::class, [
                'attr'        => [
                    'data-type' => 'end',
                    'title'     => 'Do',
                ],
                'required'    => !$options['infinite_end'],
                'label'       => $options['end_title'],
                'constraints' => $endConstraints,
            ])
            ->setDataMapper($this);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => TimeRange::class,
            'infinite_start' => false,
            'infinite_end'   => false,
            'start_title'    => 'Od',
            'end_title'      => 'Do',
            'attr'           => [
                'class' => 'customis-form-time-range',
            ],
            'empty_data'     => null,
            'constraints'    => new Valid(),
        ]);

        $resolver->setAllowedValues('infinite_start', [true, false]);
        $resolver->setAllowedValues('infinite_end', [true, false]);
    }

    /**
     * @param TimeRange                    $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        if (null !== $data) {
            $forms['start']->setData($data->getStart() ?? $data->getStart());
            $forms['end']->setData($data->getEnd() ?? $data->getEnd());
        }
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param TimeRange                    $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new TimeRange(
            $forms['start']->getData(),
            $forms['end']->getData()
        );
    }
}
