<?php

declare(strict_types = 1);

namespace CustomIS\DoctrineBundle\Form\DataTransformer;

use Brick\Math\BigDecimal;
use Brick\Math\BigInteger;
use Brick\Math\BigNumber;
use Brick\Math\Exception\NumberFormatException;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer as BaseNumberToLocalizedStringTransformer;

/**
 * Class NumberToLocalizedStringTransformer
 */
class NumberToLocalizedStringTransformer extends BaseNumberToLocalizedStringTransformer
{
    /**
     * @param float|int|BigNumber $value
     *
     * @return string
     */
    public function transform($value)
    {
        return parent::transform($value instanceof BigNumber ? (string) $value : $value);
    }

    /**
     * @param string $value
     *
     * @return BigNumber
     */
    public function reverseTransform($value)
    {
        if (!is_string($value)) {
            throw new TransformationFailedException('Expected a string.');
        }

        if ('' === $value) {
            return;
        }

        if ('NaN' === $value) {
            throw new TransformationFailedException('"NaN" is not a valid number');
        }

        $position = 0;
        $formatter = $this->getNumberFormatter();
        $groupSep = $formatter->getSymbol(\NumberFormatter::GROUPING_SEPARATOR_SYMBOL);
        $decSep = $formatter->getSymbol(\NumberFormatter::DECIMAL_SEPARATOR_SYMBOL);

        if ('.' !== $decSep && (!$this->grouping || '.' !== $groupSep)) {
            $value = str_replace('.', $decSep, $value);
        }

        if (',' !== $decSep && (!$this->grouping || ',' !== $groupSep)) {
            $value = str_replace(',', $decSep, $value);
        }

        try {
            if (false !== strpos($value, $decSep)) {
                if ('.' !== $decSep) {
                    $value = str_replace($decSep, '.', $value);
                }

                return BigDecimal::of($value);
            } else {
                return BigInteger::of($value);
            }
        } catch (NumberFormatException $e) {
            throw new TransformationFailedException($e->getMessage(), 0, $e);
        }
    }
}
