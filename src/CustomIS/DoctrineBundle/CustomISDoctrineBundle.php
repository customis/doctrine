<?php

declare (strict_types = 1);

namespace CustomIS\DoctrineBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CustomISDoctrineBundle
 *
 * @package CustomIS\DoctrineBundle
 */
class CustomISDoctrineBundle extends Bundle
{
}
